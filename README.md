# React Native - To Be or Not To Be?

![](./images/Android-Vs-iOS.jpg)

## Introduction

This setup is prepared for [**Visual Studio Code**](https://code.visualstudio.com/) as Editor and [**TypeScript**](https://github.com/Microsoft/TypeScript/blob/master/doc/spec.md) as Developement language.

## TypeScript

1. [TypeScript tutorial](http://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
2. [TypeScript Language Specification](https://github.com/Microsoft/TypeScript/blob/master/doc/spec.md)
3. 👾 [React Native template with TypeScript](https://github.com/emin93/react-native-template-typescript)
4. [Working with JSON](http://choly.ca/post/typescript-json/)

## React/JSX Style Guide
1. [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)
2. [Khan Academy ReactJS Style Guide](https://github.com/Khan/style-guides/blob/master/style/react.md)
3. [Awesome React Native](https://github.com/jondot/awesome-react-native)

#### Testing
1. [React Dev Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
2. [Enzyme: JavaScript Testing utilities for React](https://medium.com/airbnb-engineering/enzyme-javascript-testing-utilities-for-react-a417e5e5090f)

## Installation
1. Install [Visual Studio Code](https://code.visualstudio.com/) and recommended extensions:
    - [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint): take care your TypeScript quality.
    - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint): enables better code quality by bringing the ESLint library.
    - [React Native Tools](https://marketplace.visualstudio.com/items?itemName=vsmobile.vscode-react-native): lets you edit, develop, debug and integrate commands for React Native.
    - [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode): Code formatter.
    - [Auto Import](https://marketplace.visualstudio.com/items?itemName=steoates.autoimport): automatically finds parses and provides you with code actions and code completions for every import you have
    - [VScode-Icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons): adds icons to VS Code, making it more visually appealing and fun to work in.
    - [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager): makes the navigation process seamless, allowing you to multitask like a pro.

2. The React Native setup based on the [Getting Started](https://facebook.github.io/react-native/docs/getting-started.html) at `Building Projects with Native Code`.

3. React Native Package Manager:
You can use one of the following options, or use both of them -- they are in-sync:
    - [npm](https://docs.npmjs.com/cli/install): only checked in `package.json` and asked engineers to manually run `npm install`.
    - [Yarn](https://yarnpkg.com/en/): is a new package manager that replaces the existing workflow for the npm client or other package managers while remaining compatible with the npm registry. 
    
## Initial setup project
#### 1. Generate a new React Native project
##### 1.1. Using pure JavaScript:

```react-native init MyApp```

##### 1.2. Using React Native template for a quick start with TypeScript:

```react-native init MyApp --template typescript && node MyApp/setup.js```

>   **IMPORTANT:** This created among others an Android project with the package `com.myapp`. If you want to change the package name, for example to: com.mycompany.myapp -- you can choose one of options: 
>   * Use the [react-native-rename npm package](https://www.npmjs.com/package/react-native-rename)
>   * [Manually create your package, then setup to new one](https://stackoverflow.com/questions/37389905/change-package-name-for-android-in-react-native)
  
#### 2. [Integration with Existing Apps](https://facebook.github.io/react-native/docs/integration-with-existing-apps)
#### 3. Install all dependencies
```npm install```    
#### 4. Add a JavaScript libary, for example `flow-typed`

```
    npm install -g flow-typed
    react-native link
```

#### 5. (React Native Android) Ask permission to read network status
If writing a native app for Android, you'll need to make sure to request the permission to access network state in your `AndroidManifest.xml`:
```
  <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

## Visual Studio Code

#### 1. Debugging

1. Setting up debug environment: https://github.com/Microsoft/vscode-react-native/blob/master/doc/debugging.md
2. Debugging using VS Code: https://code.visualstudio.com/docs/editor/debugging
3. More info: https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_debugging

#### 2. User Settings
Open User Settings page (shortcut: `⌘,`, menu: `Code > Preferences > Settings`)

- Show file containing folder in tab: `"workbench.editor.labelFormat": "short"`
- Format on paste: `"editor.formatOnPaste": true`
- Render whitespace: `"editor.renderWhitespace": "all"`
- And many, many [other customizations](https://code.visualstudio.com/docs/getstarted/settings).

## Terminal Commands

#### 1. iOS 

| Command                                        | Description                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| `react-native run-ios`                         | Build and run the app on the default iOS simulator. It is "iPhone 6" |
| `xcrun simctl list devices`                    | List all available simulators                                 |
| `react-native run-ios —simulator="SIMULATOR_NAME"` | Build and run the app on a specific physical iOS device |

#### 2. Android

| Command                                        | Description                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| `emulator -list-avds`                          | List all your emulators                                 |
| `$ANDROID_HOME/tools/emulator @avd_name`<br/>`$ANDROID_HOME/tools/emulator -avd avd_name`  | Run one of the listed emulators. Don't forget '@' before name |
| `react-native run-android`                     | Build and run the app on your emulator.**You have to start emulator first**. |

#### 3. Others

| Command                                        | Description                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| `kill $(lsof -t -i:PORT_NUMBER)`               | Kill server by port                                     |

## Tips & Tricks
### 1. Upgrade React Native
You should consider using the new upgrade tool based on Git. It makes upgrades easier by resolving most conflicts automatically.
To use it:
- Go back to the old version of React Native
- Run ```npm install -g react-native-git-upgrade```
- Run ```react-native-git-upgrade```

See https://facebook.github.io/react-native/docs/upgrading.html react-native version in "package.json" doesn't match the installed version in "node_modules". Try running "npm install" to fix this.

### 2. Import app classes

```javascript
    // Bad
    import { ThemeContext } from "../../../lib/contexts/ThemeContext";
    import { Director, FilmSeries } from "../../../lib/types/filmTypes";

    // Good
    import { ThemeContext } from "app/lib/contexts/ThemeContext";
    import { Director, FilmSeries } from "app/lib/types/filmTypes";
    
    // Expected
    import { ThemeContext } from "app/lib/contexts";
    import { Director, FilmSeries } from "app/lib/types";
```

##### 2.1. To have the `Good` state of import:
Assume you have a folder named `app` that contains all your app typescript code

1. Open tsconfig.json (it's in the project root folder)
2. Add `"baseUrl": "./"`
3. Add `package.json` to the `app` folder with content: `{"name": "app"}`.

At this point, you can do absolute imports in your app. That means instead of the ugly `../../../foo/bar`, you can just write `app/foo/bar` for example

##### 2.2. To have the `Expected` state of import:
Assume you have this file path `app/components/Button/Button.ts`, and you want to make an import like: `import { Button } from "app/components"`

Add `index.ts` to the folder `components` with content:
```javascript
import { Button } from "./Button/Button"
export { Button };
```

### 3. Unable to load script from assets index.android.bundle

Option 1: Resolve the problem in following steps.
```
cd (project directory) 
mkdir android/app/src/main/assets
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
react-native run-android
```
We can automate the above steps by placing them in scripts part of package.json like this:
```
"android-linux": "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res && react-native run-android"
```

Option 2: Execute ```npm run android-linux``` from your command line every time.

### 4. Error in Xcode no link with libReact.a and others related libraries

Deleting derived data in File → Project Setting and restarting Xcode seems to do a trick.

It still doesn't work, try ↓

    1. On Xcode select the project → Build Settings → Set Build Active Architecture Only to Yes.
    2. Repeat same for target.
    3. Run.
    4. Set Build Active Architecture Only back to No if you want your previous configuration.

🙄 → If those options don't work, try to revert all changes(via Git) or create new xcode project 😫

### 5. Fix "No bundle url present in iOS" or "ERROR  Cannot read property 'prev' of undefined"

Info.plist → information Property List → + App Transport Security Settings → + Allow Arbitrary Loads = YES

Do not forget to do ```react-native link``` if you use any library that needs that.

### 6. Fix "Can't find variable: React"

You must `import React from 'react'` package wherever jsx is used.

```javascript
import React, {Component} from 'react';
import {
View,
...
} from 'react-native';
```

### 7. [Re-Layout on orientation change](https://github.com/facebook/react-native/issues/25)

`Dimensions.get('window')` seems to return fixed numbers and set them, so if you use that you will need to force the component to rerender on rotation.
For example:

```javascript
let {width, height} = Dimensions.get('screen');

class Peace extends Component {
    constructor() {
        super();
        this.state = {
            width,
            height
        };
    }

    _handleLayout = event => {
        this.setState({
            width: event.nativeEvent.layout.width,
            height: event.nativeEvent.layout.height
        });
    }

    render() {
        width = this.state.width;
        height = this.state.height;
        /* this should re-render our component and assign to outer scope vars */

        return (
            <View style={styles.container} onLayout={this._handleLayout}>
                ...
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width,
        height
    }
});
```

### 8. Android gradle - forces all the submodules to use the specified `compileSdkVersion` and `buildToolsVersion`
Adding the following to your root build gradle `android/build.gradle`
(**Not** the `android/app/build.gradle`)

```
subprojects {
    afterEvaluate {project ->
        if (project.hasProperty("android")) {
            android {
                compileSdkVersion 25
                buildToolsVersion '25.0.0'
            }
        }
    }
}
```

### 9. npm ERR! realm@1.2.0 install: node-pre-gyp install --fallback-to-build

Try to use `node@8` and manually installing `node-pre-gyp` with:

```
npm install --save node-pre-gyp

```

### 10. Fix "Native module cannot be null" after adding [react-native-push-notification](https://github.com/zo0r/react-native-push-notification)
- You do really need to go through manual procedure to get it work [Manual linking](https://facebook.github.io/react-native/docs/linking-libraries-ios.html#manual-linking)
- Rebuilding and running the app from Xcode

### 11. Print: Entry, ":CFBundleIdentifier", Does Not Exist

Try one of options at [#14423](https://github.com/facebook/react-native/issues/14423)

## Contributing

Contributions are very welcome 🎉 😌
